// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BasePawn.h"
#include "Tank.generated.h"

/**
 *
 */
UCLASS()
class TOONTANKS_API ATank final : public ABasePawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ATank();

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent *PlayerInputComponent) override;

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void HandleDestruction();

	APlayerController *GetTankPlayerController() const { return TankPlayerController; }

	bool bAlive = true;

protected:
	virtual void BeginPlay() override;

private:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Tank Properties", meta = (AllowPrivateAccess = "true"))
	float Speed = 200;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Tank Properties", meta = (AllowPrivateAccess = "true"))
	float TurnSpeed = 100;

	UPROPERTY(VisibleAnywhere, Category = "Camera Properties")
	class USpringArmComponent *SpringArmComponent;

	UPROPERTY(VisibleAnywhere, Category = "Camera Properties")
	class UCameraComponent *CameraComponent;

	void Move(float Value);
	void Turn(float Value);

	APlayerController *TankPlayerController;
};
