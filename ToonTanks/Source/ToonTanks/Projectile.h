// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Projectile.generated.h"

UCLASS()
class TOONTANKS_API AProjectile final : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AProjectile();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = "Projectile Properties")
	UStaticMeshComponent *ProjectileStaticMesh;

	UPROPERTY(VisibleAnywhere, Category = "Projectile Properties")
	UParticleSystemComponent *ParticleSystemComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = "Projectile Properties")
	class UProjectileMovementComponent *ProjectileMovementComponent;

	UPROPERTY(EditAnywhere, Category = "Projectile Properties")
	float InitialSpeed = 1000;

	UPROPERTY(EditAnywhere, Category = "Projectile Properties")
	float MaxSpeed = 10000;

	UPROPERTY(EditAnywhere, Category="Sounds")
	USoundBase* LaunchSound;

	UPROPERTY(EditAnywhere, Category="Sounds")
	USoundBase* HitSound;

	UPROPERTY(EditAnywhere, Category="Camera")
	TSubclassOf<UCameraShakeBase> HitCameraShakeClass;

	

	UFUNCTION()
	void OnHit(
		UPrimitiveComponent *HitComp,
		AActor *OtherActor,
		UPrimitiveComponent *OtherComp,
		FVector NormalImpulse,
		const FHitResult &Hit);

	UPROPERTY(EditAnywhere)
	float Damage = 10.0f;

	UPROPERTY(EditAnywhere, Category="Projectile Properties")
	UParticleSystem* HitParticles;
};
